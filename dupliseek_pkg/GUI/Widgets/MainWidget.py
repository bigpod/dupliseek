import math
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QImageReader, QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QScrollArea, QSizePolicy


class MainWidget(QScrollArea):
	def __init__(self, main_window):
		QLabel.__init__(self, main_window)
		self._image_label = QLabel(self)
		self._image_label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
		self._image_label.setScaledContents(True)
		self.setWidget(self._image_label)

	def show_picture(self, path):
		reader = QImageReader(path)
		reader.setAutoTransform(True)
		newImage = reader.read()
		pixMap = QPixmap()
		pixMap.convertFromImage(newImage)
		self._image_label.setPixmap(pixMap)
		self._image_label.adjustSize()
		self.resizeEvent(None)


	def resizeEvent(self, QResizeEvent):
		if self._image_label.pixmap() != None:
			image_size = self._image_label.pixmap().size()
			if image_size.width()*image_size.height() == 0:
				return
			factor = min((self.width()-2) / image_size.width(), (self.height()-2) / image_size.height())
			self._image_label.resize(self._image_label.pixmap().size() * factor)